import { Component, OnInit } from '@angular/core';
import { UploadEvent, FileSystemFileEntry } from 'ngx-file-drop';

export class Account {
	fName:string;
	lName:string;
	age:number;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

	dataTable:any;

	constructor() {}

	dropped(event: UploadEvent) {
		for (const droppedFile of event.files) {
			if (droppedFile.fileEntry.isFile) {
				const fileEntry = droppedFile.fileEntry as FileSystemFileEntry;
				fileEntry.file((file: File) => {
					this.parseFile(file);
				});
			}
		}
	}

	parseFile(file) {
		let self = this;
		let f = file;

		var reader = new FileReader();

		reader.onload = (function (theFile) {
			return function (e) {
				var json = JSON.parse(e.target.result);
				self.extractJSON(json)
			}
		})(f);
		reader.readAsText(f);
	}

	extractJSON(json) {
		let self = this;
		let newdata = [];
		for (let i = 0; i < json.length; i++) {
			let item = json[i];
			newdata.push([item.fName, item.lName, item.age]);
		}
		this.dataTable.rows.add(newdata).draw();
	}

	ngOnInit() {
		this.dataTable = $('#account-table').DataTable({
			responsive: true,
			pagingType: 'full_numbers',
			pageLength: 10,
			processing: true
		});
	}
}
